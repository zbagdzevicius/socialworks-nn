import json

from tensorflow.keras.models import load_model

from socialworks_nn import QuestionAnswerer

with open('questions_responses_data/QA.json') as file:
    data = json.load(file)

model = load_model('model-2020-05-10.h5')
question_answerer = QuestionAnswerer(data, model)

question = "hi"
question1 = "I do have a problem"
result = question_answerer.get_answer(question)
result1 = question_answerer.get_answer(question1)

print(result)
print(result1)
