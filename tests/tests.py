from socialworks_nn import QuestionAnswerer
import json


with open('questions_responses_data/questions_responses-2020-04-17.json') as file:
    data = json.load(file)

question_answerer = QuestionAnswerer(data,
                                     'oleandras.serveriai.lt',
                                     'cronjob@cronicler.com',
                                     'croniclercronjob')

question = "What do you do"

print(question_answerer.get_answer(question))