from nltk import word_tokenize, download
from tensorflow.keras.preprocessing.text import Tokenizer


class DataProcessor:

    def __init__(self, json_data):
        self.data = json_data
        download('punkt')

    def _get_preprocessed_questions(self):
        processesed_questions = []
        for intent in self.data['intents']:
            for pattern in intent['patterns']:
                processesed_questions.append(pattern)
        return processesed_questions

    def get_tags(self):
        tags = []
        for intent in self.data['intents']:
            if intent['tag'] not in tags:
                tags.append(intent['tag'])
        return sorted(tags)

    def _get_sentences(self):
        sentences = []
        for intent in self.data['intents']:
            for pattern in intent['patterns']:
                words = word_tokenize(pattern)
                sentences.append(words)
        return sentences

    def _get_sentence_tags(self):
        sentence_tags = []
        for intent in self.data['intents']:
            for _ in intent['patterns']:
                sentence_tags.append(intent['tag'])
        return sentence_tags

    def create_tokenizer_obj(self):
        processesed_questions = self._get_preprocessed_questions()

        tokenizer_obj = Tokenizer(filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n', lower=True)
        tokenizer_obj.fit_on_texts(processesed_questions)

        return tokenizer_obj

    def get_max_length(self):
        processesed_questions = self._get_preprocessed_questions()
        max_length = max([len(s.split()) for s in processesed_questions])

        return max_length
