import random

import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences

from .data_preprocessing.data_processing import DataProcessor


class QuestionAnswerer(DataProcessor):

    def __init__(self, json_data, model):
        super().__init__(json_data)

        self.model = model
        self.tokenizer = self.create_tokenizer_obj()

    def get_answer(self, question):
        tags = self.get_tags()

        max_length = self.get_max_length()

        words = [question]
        x_test_tokens = self.tokenizer.texts_to_sequences(words)
        x_test_pad = pad_sequences(x_test_tokens, maxlen=max_length, padding='post')

        results = self.model.predict(x_test_pad)
        results_index = tf.math.argmax(results[0])
        results_index = tf.keras.backend.eval(results_index)
        tag = tags[results_index]

        for intent in self.data['intents']:
            if intent['tag'] == tag:
                responses = intent['responses']
                return random.choice(responses)

        return None
#
