from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='socialworks_nn',
      packages=find_packages(),
      description='Chatbot',
      long_description=long_description,
      version='0.0.8',
      url='https://gitlab.com/zbagdzevicius/socialworks-nn',
      author='Arturas Druteika',
      author_email='arturas.druteika@gmail.com',
      keywords=['chatbot', 'neural networks', 'deep learning'],
      install_requires=['tensorflow-cpu',
                        'nltk',
                        'numpy',],
      classifiers=[
          "Programming Language :: Python :: 3",
          "Operating System :: OS Independent",
      ],
      license='Apache License 2.0',
      package_data={'data': ['*.json'],}
      )
